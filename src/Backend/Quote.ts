export const quote = ( f: (text: string)=>void) => {
  const url = "https://jsonp.afeld.me/?url=http://quotes.stormconsultancy.co.uk/random.json";
  fetch(url)
    .then(res => res.json())
    .then(
      (result) => {
        console.log(result);
        f(result.quote);
      },
      (error) => {
        console.log(error);
      });
};
