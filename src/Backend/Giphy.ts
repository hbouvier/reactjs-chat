export const giphy = ( f: (url: string)=>void, tag?: string) => {
  const url = "https://api.giphy.com/v1/gifs/random?api_key=dc6zaTOxFJmzC";
  const fullURL = tag ? `${url}&tag=${tag}` : url;
  fetch(fullURL)
    .then(res => res.json())
    .then(
      (result) => {
        console.log(result);
        f(result.data.image_url);
      },
      (error) => {
        console.log(error);
      });
};
