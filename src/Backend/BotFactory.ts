
const cannedResponses = 
  [ "Hi there, I'm Henri and you?"
  , "Nice to meet you"
  , "How are you?"
  , "Not too bad, thanks"
  , "What do you do?"
  , "That's awesome"
  , "React is the way to go"
  , "I think you're a nice person"
  , "Why do you think that?"
  , "Can you explain?"
  , "Anyway I've gotta go now"
  , "It was a pleasure chat with you"
  , "Time to make a new React project"
  , "Bye"
  , ":)"
  ];

const BotFactory = () => {
  let index = -1;
  return () => {
    index += 1;
    return index < cannedResponses.length ? cannedResponses[index] : "...";
  };
};

export default BotFactory;