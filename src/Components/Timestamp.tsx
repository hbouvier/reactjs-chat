import React from 'react';
import TimeAgo from 'react-timeago'

export type TimestampProps = { timestamp: Date };


const Timestamp = ( { timestamp } : TimestampProps) =>
<div className="timestamp">
  <TimeAgo date={timestamp} />
</div>
;

export default Timestamp;
