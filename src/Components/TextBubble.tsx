import React from 'react';

export type TextBubbleProps = {
  message: string
};

const TextBubble = ( { message } : TextBubbleProps) => message.length > 120 ? 
  <>{message.substr(0, 120)+"..."}</> :
  <>{message}</>
;

export default TextBubble;
