import React from 'react';
import CardBubble, { CardBubbleProps } from './CardBubble';
import TextBubble, { TextBubbleProps } from './TextBubble';
import Timestamp, { TimestampProps } from './Timestamp';

export enum Source {
  Bot,
  Human
};

export type Widget = CardBubbleProps | TextBubbleProps | TimestampProps;

export type BubbleProps = {
  source: Source
  widget: Widget
  loading: boolean
  timestamp?: Date
  delivered?: boolean
  read?: boolean
}

const isTextBubble = (widget: Widget) => {const r = (widget as TextBubbleProps).message !== undefined ? true : false; console.log('istext:', r, 'w:', widget); return r;}

const Bubble = (props: BubbleProps) => {
  console.log(props)
  return props.source === Source.Bot ?
    <div className={props.loading ? "message loading new":"message"}>
      <figure className="avatar">
        <img src="https://www.gravatar.com/avatar/d63acca1aeea094dd10565935d93960b" alt="bot"/>
      </figure>
      <span>
        <Message {...props.widget} />
      </span>
      {props.timestamp ? <Timestamp timestamp={props.timestamp} /> : <></>}
      {props.read ? <div className="checkmark-read">✓</div> :
       props.delivered ? <div className="checkmark-sent-delivered">✓</div> :
       <></>}
    </div>
  : props.source === Source.Human ?
    <div className="message message-personal new">
      <Message {...props.widget} />
      {props.timestamp ? <Timestamp timestamp={props.timestamp} /> : <></>}
      {props.read ? <div className="checkmark-read">✓</div> :
       props.delivered ? <div className="checkmark-sent-delivered">✓</div> :
       <></>}
    </div>
  : 
  <Timestamp {...(props.widget as TimestampProps)} />
  ;
};

const Message = (props: Widget) => isTextBubble(props) ?
  <TextBubble {...(props as TextBubbleProps)} />
  :
  <CardBubble {...(props as CardBubbleProps)} />
;

export default Bubble;