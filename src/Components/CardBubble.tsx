import React from 'react';

export type CardBubbleProps = {
  url: string
};

const CardBubble = ( { url } : CardBubbleProps) =>
<div>
  <img src={url} alt="card" width="260" height="180"/>
</div>;
export default CardBubble;
