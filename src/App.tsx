import React, { useState } from 'react';
import './App.css';
import Bubble, { BubbleProps, Source } from './Components/Bubble';
import BotFactory from './Backend/BotFactory';
import { giphy } from './Backend/Giphy';
import { quote } from './Backend/Quote';

const bot = BotFactory();
const scrollToBottom = (delay: number) => setTimeout(() => {try { document.getElementById("messages-content-div")!.scrollTop = document.getElementById("messages-content-div")!.scrollHeight } catch(e) {}}, delay);
const initialMessagesState : BubbleProps[] = [
  {source: Source.Bot, loading: true, timestamp: new Date(), widget:{message: bot()}}
];
const App = () => {
  const [ message, setMessage ] = useState("");
  const [ messages, setMessages ] = useState(initialMessagesState);

  const onMessageChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setMessage(e.target.value);
  };
  const onKeyPress = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.keyCode === 13) {
      sendMessage(message);
    }
  };

  const onClick = (e: React.MouseEvent<HTMLButtonElement>) => sendMessage(message);

  const sendMessage = (message: string) => {
    if (message !== "") {
      const msgUndelivered = {source: Source.Human, loading: false, timestamp: new Date(), delivered: false, read: false, widget: {message: message}};
      const msg = {source: Source.Human, loading: false, timestamp: new Date(), delivered: true, read: true, widget: {message: message}};
      setMessages([...messages, msgUndelivered]);
      setMessage("");

      const captured = message.match(/^\/gif(?:\s+(.*))?$/);
      if (captured) {
        giphy( (url: string) => {
          setMessages([...messages, msg, {source: Source.Bot, loading: true, timestamp: new Date(), widget:{url: url}}]);
        }, captured[1]);
      } else if (message === "/quote" ) {
        quote( (text: string) => {
          setMessages([...messages, msg, {source: Source.Bot, loading: true, timestamp: new Date(), widget:{message: text}}]);
        })
      } else if (message === "/help" ) {
        setTimeout( () => {botResponse(msg, "/quote OR /gif [subject]")}, 1);
      } else {
        setTimeout( () => {botResponse(msg, bot())}, 1500);
      }
    }
  };

  const botResponse = (msg: BubbleProps, resp: string) => {
    setMessages([...messages, msg, {source: Source.Bot, loading: true, timestamp: new Date(), widget:{message: resp}}]);
  }

  scrollToBottom(1);
  if (messages.filter( m => m.loading || m.delivered === false || m.read === false).length > 0) {
    setTimeout( () => {
      setMessages(messages.map( 
        (m) => m.loading ? {...m, loading: false} :
          m.delivered === false ? {...m, delivered: true} :
          m.read === false ? {...m, read: true} :
          m
      ));
    }, 100);
  }
  return (
    <div className="App">
      <section className="avenue-messenger">
        <div className="menu">
          <div className="items">
            <span>
              <a href="/#" title="Minimize">&mdash;</a>
              <br/>
              <a href="/#" title="End Chat">&#10005;</a>
            </span>
            <div className="button"> ... </div>
          </div>
        </div>
        <div className="agent-face">
          <div className="half">
            <img className="agent circle" src="https://www.gravatar.com/avatar/d63acca1aeea094dd10565935d93960b" alt="Henri Bouvier"/>
          </div>
        </div>
        <div className="chat">
          <div className="chat-title">
            <h1>Henri Bouvier</h1>
            <h2>React Grasshopper</h2>
          </div>
          <div className="messages">
            <div className="messages-content" id="messages-content-div">

              {messages.map((msg, i) =>
                <Bubble 
                  key={i}
                  {...msg}
                />)}

            </div>
          </div>
          <div className="message-box">
            <input className="message-input" placeholder="Type message..." onKeyDown={onKeyPress} onChange={onMessageChange} value={message}/>
            <button className="message-submit" onClick={onClick}> Send </button>
          </div>
        </div>

      </section>
    </div>
  );
}

export default App;
